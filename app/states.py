#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from re import compile

# States of mainwindow
STATES = {
    'add': {
        'button': 'button_accept',
        're': compile(r'^(\*[0-9]*|[0-9]+)$|-|/|\*[0-9]*[.]*'),
    },
    'accept': {
        'button': 'button_checkout',
        're': compile(r'^[0-9]+$'),
    },
    'payment': {
        'button': 'button_payment_method',
        're': compile(r'^[0-9]+$'),
    },
    'checkout': {
        'button': None,
        're': compile(r'^[0-9]+(,[0-9]{,2})?$')
    },
    'paid': {
        'button': None,
        're': compile(r'^(\*[0-9]*|[0-9]+)$'),
    },
    'cancel': {
        'button': None,
        're': compile(r'^(\*[0-9]*|[0-9]+)$'),
    },
    'disabled': {
        'button': None,
        're': compile(r'^(\*[0-9]*|[0-9]+)$'),
    },
    'finished': {
        'button': None,
        're': compile(r'^(\*[0-9]*|[0-9]+)$'),
    },
}

RE_SIGN = {
    'quantity': compile(r'\d+|\.\d+|\d+\.'),
}
