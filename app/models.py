
MODELS_RESTAURANT = {
    'ir.module': {
        'rec_name': 'name',
        'fields': [
            'name', 'state', 'version'
        ]
    },
    'company.company': {
        'rec_name': 'rec_name',
        'fields': [
            'party', 'logo'
        ],
        'binaries': ['logo']
    },
    'res.user': {
        'rec_name': 'name',
        'fields': ['name', 'sale_device', 'type_pos_user', 'shop', 'shops']
    },
    'product.category': {
        'rec_name': 'name',
        'fields': [
            'name', 'parent', 'childs', 'name_icon', 'accounting'
        ]
    },
    'sale.sale': {
        'rec_name': 'number',
        'fields': [
            'number', 'party.name', 'lines', 'sale_date', 'state',
            'total_amount_cache', 'salesman.rec_name',
            'payment_term.name', 'payments', 'tip_amount',
            'total_amount', 'residual_amount', 'paid_amount', 'untaxed_amount',
            'tax_amount', 'delivery_charge', 'price_list', 'invoice_number',
            'shipment_address', 'kind', 'shop', 'order_status',
            'delivery_party', 'reference', 'comment', 'payment_method',
            'delivery_state', 'table_assigned.name', 'invoice_type',
            'net_amount', 'delivery_amount',
            'source.rec_name', 'position', 'payment_term.payment_type',
            'consumer.name', 'consumer.phone', 'consumer.rec_name',
            'consumer.address', 'consumer.notes', 'consumer.delivery',
        ]
    },
    'sale.line': {
        'rec_name': 'product',
        'fields': [
            'product.template.sale_price_w_tax', 'type',
            'quantity', 'unit_price_w_tax', 'product.description', 'note',
            'description', 'qty_fraction', 'amount_w_tax', 'unit.symbol',
            'product.name', 'product.code', 'unit.digits', 'amount',
            'discount_rate', 'product.sale_price_w_tax', 'product.quantity',
            'order_sended', 'product.sale_price_taxed', 'sale'
        ]
    },
    'account.statement.journal': {
        'rec_name': 'number',
        'fields': ['name', 'require_voucher', 'kind']
    },
    'company.employee': {
        'rec_name': 'rec_name',
        'fields': ['party', 'code', 'rec_name']
    },
    'commission.agent': {
        'rec_name': 'rec_name',
        'fields': [
            # 'active',
            'party.id_number', 'party.name', 'rec_name', 'plan.percentage'
        ]
    },
    'sale.delivery_party': {
        'rec_name': 'rec_name',
        'fields': [
            'active', 'party', 'party.id_number', 'party.phone', 'rec_name',
            'number_plate', 'type_vehicle',
        ]
    },
    'sale.discount': {
        'rec_name': 'rec_name',
        'fields': ['active', 'name', 'rec_name', 'discount']
    },
    'sale.source': {
        'rec_name': 'name',
        'fields': [
            'id', 'name', 'party', 'party.invoice_address',
            'party.shipment_address'
        ]
    },
    'party.consumer': {
        'rec_name': 'rec_name',
        'fields': [
            'id_number', 'rec_name', 'name', 'address',
            'phone', 'notes', 'delivery']
    },
    'account.invoice.payment_term': {
        'rec_name': 'name',
        'fields': ['name', 'active', 'payment_type']
    },
    'sale.device': {
        'rec_name': 'name',
        'fields': [
            'name', 'shop.company', 'shop.name', 'shop.taxes',
            'shop.party.name', 'journals.name', 'journals.require_voucher',
            'journals.kind', 'shop.taxes.name',
            'shop.product_categories.accounting',
            'shop.product_categories.name',
            'shop.product_categories.name_icon',
            'shop.product_categories.parent',
            'shop.product_categories.childs.accounting',
            'shop.product_categories.childs.name_icon',
            'shop.product_categories.childs.parent',
            'shop.product_categories.childs.name',
            'shop.product_categories.childs',
            'shop.product_categories.products.id',
            'shop.product_categories.products.name',
            'shop.product_categories.products.code',
            'shop.product_categories.products.products_mix',
            'shop.product_categories.childs.products.id',
            'shop.product_categories.childs.products.name',
            'shop.product_categories.childs.products.code',
            'shop.product_categories.childs.products.products_mix',
            'journal.name', 'journal.kind',
            'journal.require_voucher',
            'shop.payment_term.name', 'shop.warehouse',
            'shop.salesman_pos_required', 'shop.electronic_authorization',
            'shop.invoice_copies', 'shop.pos_authorization', 'shop.discounts',
            'shop.computer_authorization', 'shop.manual_authorization',
            'shop.credit_note_electronic_authorization',
            'shop.salesmans.rec_name', 'shop.salesmans.code', 'shop.discount_pos_method',
            'shop.debit_note_electronic_authorization',
            'shop.delivery_man.rec_name', 'shop.price_list.name',
            'shop.order_copies', 'shop.delivery_man.active',
        ]
    },
    'sale.shop': {
        'rec_name': 'name',
        'fields': [
            'taxes', 'product_categories', 'party', 'invoice_copies',
            'warehouse', 'payment_term', 'delivery_man',
            'discounts', 'price_list', 'order_copies']
    },
    'product.product': {
        'rec_name': 'rec_name',
        'fields': [
            'name', 'code', 'write_date', 'description',
            'template.sale_price_w_tax', 'template.account_category',
            'quantity', 'list_price',
            'sale_uom.name', 'categories',
            # 'products_mix.code', 'products_mix.name'
        ],
        'binaries': ['image']
    },
    'party.party': {
        'rec_name': 'name',
        'fields': [
            'name', 'id_number', 'addresses.street', 'phone',
            'customer_payment_term.name', 'customer_payment_term.payment_type',
            'credit_limit_amount', 'receivable',
            'salesman', 'credit_amount', 'street', 'invoice_type'
        ]
    },
    'ir.action.report': {
        'rec_name': 'report_name',
        'fields': ['action', 'report_name']
    },
    'sale.configuration': {
        'rec_name': 'id',
        'fields': [
            'tip_product.code', 'show_description_pos',
            'show_position_pos', 'show_stock_pos', 'password_force_assign',
            'tip_rate', 'show_agent_pos', 'discount_pos_method', 'show_brand',
            'show_location_pos', 'show_delivery_charge', 'use_price_list',
            'decimals_digits_quantity', 'password_admin_pos', 'show_fractions',
            'new_sale_automatic', 'show_product_image',
            'no_remove_commanded', 'encoded_sale_price',
            'delivery_product.list_price', 'delivery_product.code',
            'delivery_product.name', 'allow_discount_handle',
            'cache_products_local', 'show_party_categories',
            'print_lines_product', 'uvt_pos'
        ]
    },
    'party.address': {
        'rec_name': 'name',
        'fields': [
            'name', 'street'
        ]
    },
    'sale.shop.table': {
        'rec_name': 'name',
        'fields': ['id', 'name', 'state', 'sale']
    },
    'account.tax': {
        'rec_name': 'name',
        'fields': [
            'name'
        ]
    },
    'account.statement': {
        'rec_name': 'name',
        'fields': [
            'name', 'expenses_daily', 'journal', 'turn',
        ]
    },
    'sale_pos.expenses_daily': {
        'rec_name': 'invoice_number',
        'fields': [
            'amount', 'statement', 'invoice_number', 'reference', 'party',
        ]
    },
    'commission': {
        'rec_name': 'rec_name',
        'fields': [
            'name'
        ]
    },
    'product.price_list': {
        'rec_name': 'rec_name',
        'fields': [
            'name'
        ],
    },
    'hotel.folio': {
        'rec_name': 'rec_name',
        'fields': [
            'id'
        ]
    }
}


MODELS_RETAIL = {
    'ir.module': {
        'rec_name': 'name',
        'fields': [
            'name', 'state', 'version'
        ]
    },
    'company.company': {
        'rec_name': 'rec_name',
        'fields': [
            'party', 'logo'
        ],
        'binaries': ['logo']
    },
    'res.user': {
        'rec_name': 'name',
        'fields': ['name', 'sale_device', 'type_pos_user', 'shop', 'shops']
    },
    'product.category': {
        'rec_name': 'name',
        'fields': [
            'name', 'parent', 'childs', 'name_icon', 'accounting'
        ]
    },
    'sale.sale': {
        'rec_name': 'number',
        'fields': [
            'number', 'party.name', 'lines', 'sale_date', 'state',
            'total_amount_cache', 'salesman.rec_name',
            'payment_term.name', 'payments', 'tip_amount',
            'total_amount', 'residual_amount', 'paid_amount', 'untaxed_amount',
            'tax_amount', 'delivery_charge', 'price_list', 'invoice_number',
            'shipment_address', 'kind', 'shop', 'turn',
            'delivery_party', 'reference', 'comment', 'payment_method',
            'delivery_state', 'invoice_type', 'net_amount',
            'delivery_amount', 'source.rec_name', 'position',
            'payment_term.payment_type',
        ]
    },
    'sale.line': {
        'rec_name': 'product',
        'fields': [
            'product.template.sale_price_w_tax', 'type',
            'quantity', 'unit_price_w_tax', 'product.description', 'note',
            'description', 'qty_fraction', 'amount_w_tax', 'unit.symbol',
            'product.name', 'product.code', 'unit.digits', 'amount',
            'discount_rate', 'product.sale_price_w_tax', 'product.quantity',
            'order_sended', 'product.sale_price_taxed', 'sale'
        ]
    },
    'account.statement.journal': {
        'rec_name': 'number',
        'fields': ['name', 'require_voucher', 'kind']
    },
    'company.employee': {
        'rec_name': 'rec_name',
        'fields': ['party', 'code', 'rec_name']
    },
    'commission.agent': {
        'rec_name': 'rec_name',
        'fields': [
            # 'active',
            'party.id_number', 'party.name', 'rec_name', 'plan.percentage'
        ]
    },
    'sale.delivery_party': {
        'rec_name': 'rec_name',
        'fields': [
            'active', 'party', 'party.id_number', 'party.phone', 'rec_name',
            'number_plate', 'type_vehicle',
        ]
    },
    'sale.discount': {
        'rec_name': 'rec_name',
        'fields': ['active', 'name', 'rec_name', 'discount']
    },
    'sale.source': {
        'rec_name': 'name',
        'fields': [
            'id', 'name', 'party', 'party.invoice_address',
            'party.shipment_address'
        ]
    },
    'party.consumer': {
        'rec_name': 'rec_name',
        'fields': [
            'id_number', 'rec_name', 'name', 'address',
            'phone', 'notes', 'delivery']
    },
    'account.invoice.payment_term': {
        'rec_name': 'name',
        'fields': ['name', 'active', 'payment_type']
    },
    'sale.device': {
        'rec_name': 'name',
        'fields': [
            'name', 'shop.company', 'shop.name', 'shop.taxes',
            'shop.party.name', 'journals.name', 'journals.require_voucher',
            'journals.kind', 'shop.taxes.name',
            'shop.product_categories.accounting',
            'shop.product_categories.name',
            'shop.product_categories.name_icon',
            'shop.product_categories.parent',
            'shop.product_categories.childs.accounting',
            'shop.product_categories.childs.name_icon',
            'shop.product_categories.childs.parent',
            'shop.product_categories.childs.name',
            'shop.product_categories.childs',
            'shop.product_categories.products.id',
            'shop.product_categories.products.name',
            'shop.product_categories.products.code',
            'shop.product_categories.childs.products.id',
            'shop.product_categories.childs.products.name',
            'shop.product_categories.childs.products.code',
            'journal.name', 'journal.require_voucher',
            'journal.kind', 'shop.discount_pos_method',
            'shop.payment_term.name', 'shop.warehouse',
            'shop.salesman_pos_required', 'shop.electronic_authorization',
            'shop.invoice_copies', 'shop.pos_authorization', 'shop.discounts',
            'shop.computer_authorization', 'shop.manual_authorization',
            'shop.credit_note_electronic_authorization',
            'shop.salesmans.rec_name', 'shop.salesmans.code', 'shop.delivery_man.active',
            'shop.debit_note_electronic_authorization',
            'shop.delivery_man.rec_name', 'shop.price_list.name',
            'shop.order_copies'
        ]
    },
    'sale.shop': {
        'rec_name': 'name',
        'fields': [
            'taxes', 'product_categories', 'party', 'invoice_copies',
            'warehouse', 'payment_term', 'delivery_man',
            'discounts', 'price_list', 'order_copies']
    },
    'product.product': {
        'rec_name': 'rec_name',
        'fields': [
            'name', 'code', 'write_date', 'description',
            'template.sale_price_w_tax', 'template.account_category',
            'quantity', 'list_price',
            'sale_uom.name', 'categories',
        ],
        'binaries': ['image']
    },
    'party.party': {
        'rec_name': 'name',
        'fields': [
            'name', 'id_number', 'addresses.street', 'phone',
            'customer_payment_term.name', 'customer_payment_term.payment_type',
            'credit_limit_amount', 'receivable',
            'salesman', 'credit_amount', 'street', 'invoice_type'
        ]
    },
    'ir.action.report': {
        'rec_name': 'report_name',
        'fields': ['action', 'report_name']
    },
    'sale.configuration': {
        'rec_name': 'id',
        'fields': [
            'tip_product.code', 'show_description_pos',
            'show_position_pos', 'show_stock_pos', 'password_force_assign',
            'tip_rate', 'show_agent_pos', 'discount_pos_method', 'show_brand',
            'show_location_pos', 'show_delivery_charge', 'use_price_list',
            'decimals_digits_quantity', 'password_admin_pos', 'show_fractions',
            'new_sale_automatic', 'show_product_image', 'delivery_product.code',
            'encoded_sale_price', 'delivery_product.list_price',
            'delivery_product.name', 'allow_discount_handle',
            'cache_products_local', 'show_party_categories',
            'print_lines_product', 'uvt_pos'
        ]
    },
    'party.address': {
        'rec_name': 'name',
        'fields': [
            'name', 'street'
        ]
    },
    'sale.shop.table': {
        'rec_name': 'name',
        'fields': ['id', 'name', 'state', 'sale']
    },
    'account.tax': {
        'rec_name': 'name',
        'fields': [
            'name'
        ]
    },
    'account.statement': {
        'rec_name': 'name',
        'fields': [
            'name', 'expenses_daily', 'journal', 'turn',
        ]
    },
    'sale_pos.expenses_daily': {
        'rec_name': 'invoice_number',
        'fields': [
            'amount', 'statement', 'invoice_number', 'reference', 'party',
        ]
    },
    'commission': {
        'rec_name': 'rec_name',
        'fields': [
            'name'
        ]
    },
    'product.price_list': {
        'rec_name': 'rec_name',
        'fields': [
            'name'
        ],
    },
    'hotel.folio': {
        'rec_name': 'rec_name',
        'fields': [
            'id'
        ]
    }
}
