import os
# from PyQt5.QtGui import QIcon
from PySide6.QtGui import QIcon, QGuiApplication
# from PyQt5.QtWidgets import QDesktopWidget


current_dir = os.path.dirname(__file__)


def get_icon(name):
    name = name if name else 'fork'
    path_icon = os.path.join(current_dir, 'share', name + '.svg')
    return QIcon(path_icon)


def to_numeric(number):
    return str(round(number, 2))


def to_float(number, digits):
    return str(round(number, 4))


def get_screen():
    screen = QGuiApplication.primaryScreen().size()
    # screen = QDesktopWidget().screenGeometry()
    width = screen.width()
    height = screen.height()
    return width, height

def compare_versions(version_validate, version_required):
    v1_parts = version_validate.split('.')
    v2_parts = version_required.split('.')

    for i in range(max(len(v1_parts), len(v2_parts))):
        v1_num = int(v1_parts[i]) if i < len(v1_parts) else 0
        v2_num = int(v2_parts[i]) if i < len(v2_parts) else 0

        if v1_num < v2_num:
            return "lower"
        elif v1_num > v2_num:
            return "higher"
    return 'equal'
