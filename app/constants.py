import os
from datetime import datetime
from decimal import Decimal

# from PyQt5.QtCore import Qt, QDate
# from PyQt5.QtWidgets import QHeaderView
from PySide6.QtCore import Qt, QDate
from PySide6.QtWidgets import QHeaderView

PATH_PRINTERS = '/dev/usb'

UTC_OFFSET_TIMEDELTA = datetime.now() - datetime.utcnow()
DELTA_LOCALE = round(UTC_OFFSET_TIMEDELTA.total_seconds() / 60 / 60)
RATE_CREDIT_LIMIT = 0.8

STRETCH = QHeaderView.Stretch
alignRight = Qt.AlignRight
alignLeft = Qt.AlignLeft
alignCenter = Qt.AlignCenter
alignVCenter = Qt.AlignVCenter
alignHCenter = Qt.AlignHCenter

DIALOG_REPLY_NO = 0
DIALOG_REPLY_YES = 1
ZERO = Decimal('0')

FRACTIONS = [
    ('', ''),
    ('1', '1'),
    ('0.5', '1/2'),
    ('0.25', '1/4'),
    ('0.125', '1/8'),
    ('0.0625', '1/16'),
    ('0.0313', '1/32')
]

current_dir = os.path.dirname(__file__)

SCREENS = {
    'large': os.path.join(current_dir, 'large_screen.css'),
    'medium': os.path.join(current_dir, 'medium_screen.css'),
    'small': os.path.join(current_dir, 'small_screen.css')
}

CONVERSION_DIGITS = {
    'gal': '4',
    'u': '0',
    'g': '4',
    'm': '2',
    'dm': '2',
    'km': '2',
    'kg': '2',
}

FILE_BANNER = os.path.join(current_dir, 'share', 'pos_banner.png')

MINIMUM_DATE = QDate(0, 1, 1)

MONEY = [
    [100000, 0, 0],
    [50000, 0, 0],
    [20000, 0, 0],
    [10000, 0, 0],
    [5000, 0, 0],
    [2000, 0, 0],
    [1000, 0, 0],
    [500, 0, 0],
    [200, 0, 0],
    [100, 0, 0],
    [50, 0, 0],
]

SALE_FIELDS = [
    'number', 'party', 'sale_date', 'comment', 'position',
    'description', 'invoice_number', 'tip_amount', 'delivery_amount',
    'salesman', 'state', 'kind', 'reference',
    'invoice_type', 'consumer', 'delivery_party', 'table_assigned'
]

KIND_REST = [
    # ('', ''),
    ('take_away', 'PARA LLEVAR'),
    ('delivery', 'DOMICILO'),
    ('to_table', 'A LA MESA'),
    ('catering', 'CATERING')
]

KIND_RETAIL = [
    # ('', ''),
    ('take_away', 'PARA LLEVAR'),
    ('delivery', 'DOMICILO'),
]

TYPE_VEHICLE = [
    ('', ''),
    ('motorcycle', 'Motorcycle'),
    ('bicycle', 'Bicycle'),
    ('car', 'Car'),
]

TYPE_PRINTER = [
    ('', ''),
    ('usb', 'USB'),
    ('network', 'RED'),
]

ROW_CHARACTERS = [
    ('', ''),
    ('28', '28'),
    ('33', '33'),
    ('42', '42'),
    ('48', '48'),
]