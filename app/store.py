from app.commons.forms import ComboBox
from .translation import tr


class StoreView(object):
    """
    Class for that all changes in store data update view fields and database
    """

    def __init__(self, parent, fields):
        self.parent = parent
        self.Sale = parent.Sale
        self.store = {f: None for f in fields}

    def set_sale(self, sale_id):
        self.sale_id = sale_id

    def get(self, field_name):
        field = self.store[field_name]
        return field

    def clear(self, exception=[]):
        self.sale_id = None
        for fname, v in self.store.items():
            if fname in exception:
                continue
            field = getattr(self.parent, 'field_' + fname, None)
            if field and hasattr(field, 'setText'):
                field.setText('')

            field_ask = getattr(self.parent, 'field_' + fname + '_ask', None)
            if field_ask and hasattr(field_ask, 'setText'):
                field_ask.setText('')

            self.store[fname] = None

    def set(self, values):
        if values.get('id'):
            self.sale_id = values['id']
        for k, v in values.items():
            if k.endswith('.'):
                k = k[:-1]
            self.store[k] = v
            field = getattr(self.parent, 'field_' + k, None)
            if field and hasattr(field, 'setText'):
                if isinstance(v, dict):
                    val = v.get('rec_name') or v.get('name')
                elif v is None:
                    val = ''
                else:
                    val = v
                if hasattr(field, 'translate'):
                    val = tr(val)
                field.setText(str(val))
            elif isinstance(field, ComboBox):
                field.updateComboBox(v)
                # if v:
                #     field.set_from_id(v)
                # else:
                #     field.set_none()

    def update(self, values):
        """
        This method update record in database and current view
        values: Dict
        """
        for k, v in values.items():
            self.store[k] = v
            field_name = 'field_' + k

            field = getattr(self.parent, field_name, None)
            if not field:
                continue
            if hasattr(field, 'setText'):
                if isinstance(v, dict):
                    val = v.get('rec_name') or v.get('name') or v['id']
                else:
                    val = v

                field.setText(str(val))

            if isinstance(v, dict):
                values[k] = v['id']

        res = self.Sale.write([self.sale_id], values)
        return res
