import os
import shutil
# from PyQt5.QtCore import QSettings
from PySide6.QtCore import QSettings


class Params(object):
    """
    Params Configuration
    This class load all settings from .ini file
    """

    def __init__(self, file_):
        self.file = file_

        current_path = os.path.abspath(__file__)
        dirx = os.path.abspath(os.path.join(current_path, '..', '..', '..'))

        if os.name == 'posix':
            homex = 'HOME'
            dirconfig = '.tryton'
        elif os.name == 'nt':
            homex = 'USERPROFILE'
            dirconfig = 'AppData/Local/tryton'

        HOME_DIR = os.getenv(homex)
        default_dir = os.path.join(HOME_DIR, dirconfig)

        if not os.path.exists(default_dir):
            os.makedirs(default_dir)
        
        self.config_file = os.path.join(default_dir, self.file)

        if not os.path.exists(self.config_file):
            config_file_path = os.path.join(dirx, self.file)
            shutil.copy2(config_file_path, self.config_file)

        settings = QSettings(self.config_file, QSettings.IniFormat)
        self.params = {
            "server": "localhost",
            "port": "8010",
            "mode": "http",
            "database": "DBNAME",
            "user": None,
            "sale_device_code": None,
            "printer_sale_name": None,
            "profile_printer": "TM-P80",
            "row_characters": "48",
            "print_receipt": "manually",  # manually or automatic
            "active_weighing": False,
            "print_order": False,
            "print_auto_order": False,
            "auto_print_commission": False,
            "button_to_draft_active": False,
            "server_printer": False,
            "active_timeout": True,
            "tasks_station": False,
            "timeout": 10000,
            "tablet_mode": False,
            "theme": "base",
            "mode_window": "maximized",
            "locale": "es_CO.UTF-8",
            "language": "es_CO",
            "environment": "retail",
            "profile_name": "MY DEMO"
        }
        for key in settings.allKeys():
            if key[0] == '#':
                continue
            if key == 'enviroment':  # for remove this condition
                self.params['environment'] = settings.value(key, None)
            else:
                self.params[key] = settings.value(key, None)
