
# from PyQt5.QtCore import Qt
# from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PySide6.QtCore import Qt
from PySide6.QtGui import QStandardItemModel, QStandardItem


def get_simple_model(obj, data, header=[]):
    model = QStandardItemModel(0, len(header), obj)
    if header:
        i = 0
        setHeaderData = model.setHeaderData
        for head_name in header:
            setHeaderData(i, Qt.Horizontal, head_name)
            i += 1
    _insert_items(model, data)
    return model


def _insert_items(model, data):
    appendRow = model.appendRow
    for d in data:
        row = []
        row_append = row.append
        for val in d:
            itemx = QStandardItem(str(val))
            itemx.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
            row_append(itemx)
        appendRow(row)
    model.sort(0, Qt.AscendingOrder)


def set_selection_model(tryton_model, args):
    pass
