#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import gettext
import logging
import ssl
from pathlib import Path
from PySide6.QtWidgets import QDialog
from PySide6.QtCore import Qt, QTimer
from PySide6.QtGui import QPixmap
from http.client import HTTPConnection, HTTPSConnection
import orjson as json
from app.threads import VerifyConn
from app.commons.config import Params
from app.commons.dialogs import ConfigEditDialog
from app.commons.ui_login import Ui_Login
from logger_config import logger
context_http = ssl._create_unverified_context()

_ = gettext.gettext

__all__ = ['Login', 'xconnection']

pkg_dir = str(Path(os.path.dirname(__file__)).parents[0])
path_logo = os.path.join(pkg_dir, 'share', 'login.png')
path_circle_red = os.path.join(pkg_dir, 'share', 'circle_red.svg')
path_circle_green = os.path.join(pkg_dir, 'share', 'circle_green.svg')
file_base_css = os.path.join(pkg_dir, 'css', 'base.css')
file_tablet_css = os.path.join(pkg_dir, 'css', 'tablet.css')
HEADERS = {
    "Content-type": "application/json",
    "Accept": "text/plain"
}


class Login(QDialog):

    def __init__(self, parent=None, file_config=''):
        super(Login, self).__init__(parent)
        logging.info(' Start login Neox system X...')
        self.context = {}
        params = Params(file_config)
        self.params = params.params
        self.config_file_path = params.config_file
        self.setObjectName('dialog_login')
        self.ui = Ui_Login()
        self.ui.setupUi(self)
        self.verify_conn_th = VerifyConn(self)
        self.verify_conn_th.sigVerifyConn.connect(self.verify_active)
        self.verify_conn_th.start()

    def verify_active(self):
        conn = HTTPSConnection('google.com', timeout=5)
        try:
            conn.connect()
            conn._validate_host('google.com')
            conn.close()
            option = u"online"
            icon_conn = path_circle_green
        except Exception:
            icon_conn = path_circle_red
            option = u"offline"
        self.ui.label_conn.setText(option)
        self.ui.label_icon_conn.setPixmap(QPixmap(icon_conn))
        self.timer = QTimer()
        self.timer.timeout.connect(self.verify_active)
        self.timer.start(10)
        self.verify_conn_th.exit(0)

    def set_style(self, style_files):
        styles = []
        for style in style_files:
            with open(style, 'r') as infile:
                styles.append(infile.read())
        self.setStyleSheet(''.join(styles))

    def keyPressEvent(self, event):
        print(event, 'this is event')
        if event.key() == Qt.Key_Escape:
            self.reject()
        elif event.key() == Qt.Key_Enter:
            self.accept()
        event.accept()

    def clear_message(self):
        self.error_msg.hide()

    def run(self, profile=None):
        # self.api_url = self.params['api_url']
        if self.params['database']:
            self.ui.field_database.setText(self.params['database'])
        if self.params['user']:
            self.ui.field_user.setText(self.params['user'])

    def accept(self):
        self.validate_access()
        super(Login, self).accept()

    def clicked(self, event):
        state = event.text()
        if state == 'ONLINE':
            event.setText('OFFLINE')
            event.setStyleSheet("background-color: rgb(255, 115, 0);")
            self.mode_conn = 'offline'
            self.error_msg.setText('Modo de facturacion offline activado...!')
            self.error_message()
            self.run()
        elif state == 'OFFLINE':
            event.setText('ONLINE')
            event.setStyleSheet("background-color: rgb(170, 175, 183);")
            self.mode_conn = 'online'
            self.clear_message()
            self.run()

    def reject(self):
        sys.exit()

    def validate_access(self):
        user = self.ui.field_user.text()
        password = self.ui.field_password.text()
        server = self.params.get('server')
        database = self.ui.field_database.text()
        result = xconnection(
            self.params['mode'], user, password, server, database, self.params['port']
        )
        if result['status'] != 200 or result['user'] is None:
            self.ui.field_password.setText('')
            self.ui.field_password.setFocus()
            self.run()
            msg = 'Error: usuario o contraseña invalida...!'
            if result['status'] == 429:
                msg = 'Error: Tiene muchos intentos de sesion, \npara desbloquear contacta con el area administrativa'
            elif result['status'] == 408:
                msg = 'Error: conexion del servidor'
            elif result['status'] == 500:
                msg = 'Error: interno del servidor \n' + result['message']
            logger.error(result)
            logger.info(msg)
            self.ui.label_error.setText(msg)
            self.error_message()
        else:
            self.context = result
        self.params['user'] = user
        self.params['password'] = password

    def error_message(self):
        self.ui.label_error.show()
    
    def open_config_file(self):
        # config_file_path = "/home/psk/.tryton/config_pos.ini"  # Reemplaza con tu ruta real
        dialog = ConfigEditDialog(self.config_file_path)
        dialog.exec_()

    def show_about(self):
        # Aquí debes implementar la lógica para mostrar el diálogo de configuración
        # Puedes crear una nueva ventana de configuración o un cuadro de diálogo modal.
        # Por ejemplo:
        print('pasa a mos')
        # config_dialog = ConfigDialog(self)
        # config_dialog.exec_()


def xconnection(mode, user, password, host, database, port):
    # Get user_id and session
    if mode == 'http':
        conn = HTTPConnection(host, port=port, timeout=10)
    else:
        conn = HTTPSConnection(host, port=port, timeout=10, context=context_http)
    url = '/' + database + '/fast_login'
    payload = {
        'username': user,
        'passwd': password
    }
    try:
        conn.request('POST', url, body=json.dumps(payload), headers=HEADERS)
        response = conn.getresponse()
    except Exception as e:
        status = 408
        response = None
        print(e, 'error')
    try:
        res = json.loads(response.read())
        if not res.get('status'):
            res['status'] = response.status
    except Exception:
        status = status
        msg = ''
        if response:
            status = response.status
            msg = response.msg

        res = {
            'status': status,
            'message': msg
        }

    conn.close()
    return res
