
# from PyQt5.QtWidgets import QLabel, QWidget, QDesktopWidget
# from PyQt5.QtCore import Qt, QByteArray
# from PyQt5.QtGui import QPixmap
from PySide6.QtWidgets import QLabel, QWidget, QHBoxLayout
from PySide6.QtCore import Qt, QByteArray
from PySide6.QtGui import QPixmap, QGuiApplication

__all__ = ['Image']


class Image(QLabel):

    def __init__(self, parent=None, name=None, default_img=None, scaled_rate=None):
        if not parent:
            parent = QWidget()
        super(Image, self).__init__(parent, Qt.Window)

        # screen = QDesktopWidget().screenGeometry()
        screen = QGuiApplication.primaryScreen().size()
        screen_width = screen.width()

        self.parent = parent
        self.setObjectName('img_' + name)

        if default_img:
            self.pixmap = QPixmap()
            self.pixmap.load(default_img)
            img_width, img_height = self.pixmap.width(), self.pixmap.height()
            scaled_rate = False
            if screen_width <= 1024:
                scaled_rate = 0.5
            elif screen_width <= 1366:
                scaled_rate = 0.75
            if scaled_rate:
                new_width = img_width * scaled_rate
                new_height = img_height * scaled_rate
                self.pixmap = self.pixmap.scaled(int(new_width), int(new_height),
                    Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.setPixmap(self.pixmap)

    def set_image(self, img, kind=None):
        self.pixmap = QPixmap()
        if img:
            if kind == 'bytes':
                ba = QByteArray.fromBase64(img)
                self.pixmap.loadFromData(ba)
            if kind == 'qimage':
                self.pixmap.convertFromImage(img)
            else:
                self.pixmap.loadFromData(img.data)
            self.setPixmap(self.pixmap)

    def load_image(self, pathfile):
        self.pixmap = QPixmap()
        self.pixmap.load(pathfile)
        self.setPixmap(self.pixmap)

    def activate(self):
        self.free_center()
        qlabel = QLabel()
        qlabel.setPixmap(self.pixmap)
        qhVox = QHBoxLayout()
        qhVox.addWidget(qlabel)
        self.parent.setLayout(qhVox)
        self.parent.show()

    def free_center(self):
        screen = QGuiApplication.primaryScreen().size()
        # screen = QDesktopWidget().screenGeometry()
        screen_width = screen.width()
        screen_height = screen.height()
        size = self.pixmap.size()
        self.parent.setGeometry(
            int((screen_width / 2) - (size.width() / 2)),
            int((screen_height / 2) - (size.height() / 2)),
            size.width(),
            size.height()
        )
