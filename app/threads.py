# from PyQt5.QtCore import QThread, pyqtSignal
from PySide6.QtCore import QThread, Signal


class DoInvoice(QThread):
    """
    Process invoices using a thread
    """
    # sigDoInvoice = pyqtSignal()
    sigDoInvoice = Signal()

    def __init__(self, main, context):
        QThread.__init__(self)

    def run(self):
        self.sigDoInvoice.emit()


class VerifyConn(QThread):
    """
    Verfify connection network using a thread
    """
    sigVerifyConn = Signal()

    def __init__(self, parent):
        QThread.__init__(self)

    def run(self):
        self.sigVerifyConn.emit()


class VerifyOrderCommand(QThread):
    """
    Verfify orders to command using a thread
    """
    sigVerifyOrderCommand = Signal()

    def __init__(self, parent):
        QThread.__init__(self)

    def run(self):
        self.sigVerifyOrderCommand.emit()
