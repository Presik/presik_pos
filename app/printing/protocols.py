#!/usr/bin/python
from escpos.escpos import Escpos

try:
    import paramiko
except:
    print('LOG: paramiko module not found!')


class FileSSH(Escpos):
    """ Define Generic file printer on remote machine"""

    def __init__(self, username_, password_, hostname_,  port_, devfile, *args, **kwargs):
        """
        @param devfile : Device file under dev filesystem
        """
        Escpos.__init__(self, *args, **kwargs)
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh_client.connect(hostname_, int(port_), username=username_, password=password_)
        self.sftp_client = self.ssh_client.open_sftp()
        self.devfile = devfile

    def open(self):
        """ Open system file """
        self.device = self.sftp_client.open(self.devfile, 'wb')

        if self.device is None:
            print("Could not open the specified file %s" % self.devfile)

    def _raw(self, msg):
        """ Print any command sent in raw format """
        self.device.write(msg)

    def close(self):
        """ Close system file """
        if self.device:
            self.device.close()

    def __del__(self):
        """ Close system file """
        if self.device:
            self.device.close()
