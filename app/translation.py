
dict_ = {
    'draft': 'BORRADOR',
    'commanded': 'COMANDADO',
    'in_preparation': 'EN PREPARACION',
    'dispatched': 'DESPACHADO',
    'delivered': 'ENTREGADO',
    'rejected': 'RECHAZADO',
    'cancelled': 'CANCELADO',
}


def tr(value):
    res = dict_.get(value)
    if not res:
        res = value
    return res
