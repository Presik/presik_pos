import cProfile
import pstats

# wrapper
def profile_filter(func):
    def profiled_func(*args, **kwargs):
        profiler = cProfile.Profile()
        profiler.enable()
        result = profiler.runcall(func, *args, **kwargs)

        # Captura las estadísticas
        stats = pstats.Stats(profiler)
        profiler.disable()
        stats.sort_stats('cumulative')

        # Imprime las estadísticas en la salida estándar
        stats.print_stats()

        return result
    return profiled_func
