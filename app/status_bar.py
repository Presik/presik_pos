from collections import OrderedDict
# from PyQt5.QtWidgets import QWidget, QLabel
# from PyQt5.QtCore import Qt
from PySide6.QtWidgets import QWidget, QLabel
from PySide6.QtCore import Qt

__all__ = ['StatusBar']


class StatusBar(QWidget):

    def __init__(self, parent):
        super(StatusBar, self).__init__()
        p = parent
        values = OrderedDict([
            ('stb_version', {'name': 'VERSION', 'value': p.version}),
            ('stb_shop', {'name': 'SUCURSAL', 'value': p.shop['name']}),
            ('stb_device', {'name': 'TERMINAL', 'value': p.device['name']}),
            ('stb_database', {'name': 'DB', 'value': p.database}),
            ('stb_user', {'name': 'USUARIO', 'value': p.user}),
            ('stb_printer', {'name': 'IMPRESORA',
                             'value': p.printer_sale_name})
        ])

        status_bar = p.statusBar()
        status_bar.setSizeGripEnabled(False)

        for k, v in values.items():
            _label = QLabel(v['name'] + ':')
            _label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
            _label.setObjectName('label_status_bar')
            status_bar.addWidget(_label, 1)
            setattr(parent, k, QLabel(str(v['value'])))
            _field_info = getattr(parent, k)
            _field_info.setObjectName('field_status_bar')
            _field_info.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            status_bar.addWidget(_field_info)
