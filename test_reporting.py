import os

pat = '/dev/usb/lp2'

print(os.path.exists(pat))

from app.reporting import Receipt

fake_data = {
    "12": {
        "printer": {
            "device": "192.168.1.33",
            "interface": "network",
            "profile": "",
            "row_characters": None
        },
        "sale": 'RES00234',
        "work_station": 'POSTRES',
        "lines": [
            {
                "name": "ASADO DE CARNE HAMBURGUESA",
                "description": "ASAR CARNE DE HAMBURGUESA",
                "qty": 1.0,
                "note": 'sin salsa'
            },
            {
                "name": "PREPARACION DE ENSALADA DE VEGETALES",
                "description": "ENSALADA DE VEGETALES Y VERDURAS",
                "qty": 1.0,
                "note": 'sin salsa'
            }
        ]
    },
    # "13": {
    #     "printer": {
    #         "device": "/dev/usb/lp0",
    #         "interface": "usb",
    #         "profile": "",
    #         "row_characters": 48
    #     },
    #     "sale": {
    #         "number": None
    #     },
    #     "lines": [
    #         {
    #             "name": "PREPARACION DE PAPAS",
    #             "description": "PAPAS A LA FRANCESA",
    #             "qty": 1.0,
    #             "note": 'sin salsa'
    #         }
    #     ]
    # }
}

receipt = Receipt(context={}, environment='restaurant')
for i in range(20):
    receipt.print_tasks(fake_data)
