import os
from cx_Freeze import setup, Executable

base = None

# Determine the base for the current operating system
if os.name == "posix":
    base = "Console"
elif os.name == "nt":
    base = "Win32GUI"

executables = [
    Executable("pospro", base=base)  # Reemplaza "pospro.py" con tu script principal
]

options = {
    "build_exe": {
        "packages": [
            "tests",
            "app",
            "app.printing",
            "app.commons",
            "app.css",
            "app.locale",
        ],
        "include_msvcr": True,  # Incluye las bibliotecas de tiempo de ejecución de MSVC
    }
}

setup(
    name="presik_pos",
    version="1.0",  # Reemplaza con la versión adecuada
    description="POS Client for Tryton",
    author="Oscar Alvarez",
    author_email="gerente@presik.com",
    url="www.presik.com",
    download_url="http://www.bitbucket.org",
    options=options,
    executables=executables,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: X11 Applications :: Qt",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
        "Natural Language :: English",
        "Natural Language :: Spanish",
        "Programming Language :: Python3.10",
        "Topic :: Office/Business",
    ],
    license="GPL",
    install_requires=[
        "pyusb",
        "pillow",
        "qrcode",
        "paramiko",
        "orjson",
        "escpos",
        "PySide6>=6.4.1",
        "psutil",
        "setproctitle",
    ]
)
