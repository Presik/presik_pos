import os
import logging

if os.name == 'posix':
    homex = 'HOME'
    dirconfig = '.tryton'
    temp_log = '/tmp'
elif os.name == 'nt':
    homex = 'USERPROFILE'
    dirconfig = 'AppData/Local/tryton'
    temp_log = 'AppData/Local/Temp'

HOME_DIR = os.getenv(homex)
default_dir = os.path.join(HOME_DIR, dirconfig)

if os.path.exists(default_dir):
    log_file_path = default_dir + '/presik_pos.log'
else:
    log_file_path = temp_log + '/presik_pos.log'

log_formater = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(funcName)s - %(name)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S', style="%")


handlerConsole = logging.StreamHandler()
handlerConsole.setFormatter(log_formater)

handlerFile = logging.FileHandler(log_file_path, mode='a', encoding='utf-8')
handlerFile.setFormatter(log_formater)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(handlerConsole)
logger.addHandler(handlerFile)
