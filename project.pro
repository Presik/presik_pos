# Execute in terminal $ pylupdate5 project.pro
SOURCES = app/mainwindow.py app/frontwindow.py app/status_bar.py app/stack_messages.py app/dialogs.py app/reporting.py app/buttonpad.py app/commons/custom_button.py app/commons/buttons.py app/commons/dblogin.py app/commons/dialogs.py app/commons/forms.py app/commons/menu_list.py app/commons/search_window.py app/commons/messages.py
TRANSLATIONS = app/locale/i18n_es.ts
