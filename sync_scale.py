import os

# fullpath = os.path.join(dir, name_file)


# with open(fullpath) as f:
#     lines = f.readlines()
#
#
# for line in lines:
#     print("*" * 100)
#     print("leyendo el archivo.....", name_file)
#     print(line)
#
#     code_product = line[:5]
#     weight = line[5:11]
#     unit_price = line[11:16]
#     amount = line[16:22]
#     date_ = line[22:28]
#     time_ = line[28:32]
#     salesman = line[32:37]
#     scale_code = line[37:39]
#     ticket = line[39:]
#     # code_product
#     print('code_product ', code_product)
#     print('weight ', weight)
#     print('unit_price ', unit_price)
#     print('amount ', amount)
#     print('date_ ', date_)
#     print('time_ ', time_)
#     print('salesman ', salesman)
#     print('scale_code ', scale_code)
#     print('ticket ', ticket)


def read_files_scale():
    try:
        if os.name == 'posix':
            dir = '/home/psk/Documents/DIBAL'
        else:
            dir = r'D:\USUARIOS\USUARIO\Desktop\TICKETS DIBAL'
        files = os.listdir(dir)
    except:
        dir = None
        files = []

    print('dir....', files)
    name_file = '2000150560643.txt'

    new_sales = []
    for _file in files:
        if not '.txt' in _file:
            continue

        fullpath = os.path.join(dir, _file)
        new_sale = None
        with open(fullpath) as f:
            new_sale = {}
            lines = f.readlines()
            lines_to_add = []
            for line in lines:
                print("*" * 100)
                print("leyendo el archivo.....", name_file)
                print(line)
                product = line[:5].rstrip()
                weight = line[5:11].rstrip()
                unit_price = line[11:16].rstrip()
                amount = line[16:22].rstrip()
                date_ = line[22:28].rstrip()
                time_ = line[28:32].rstrip()
                salesman = line[32:37].rstrip()
                scale_code = line[37:39].rstrip()
                ticket = line[39:].rstrip()
                print('product ', product)
                print('weight ', weight)
                print('unit_price ', unit_price)
                print('amount ', amount)
                print('date_ ', date_)
                print('time_ ', time_)
                print('salesman ', salesman)
                print('scale_code ', scale_code)
                print('ticket ', ticket)
                if not new_sale:
                    new_sale = {
                        'date_': date_,
                        'time_': time_,
                        'salesman': salesman,
                        'scale_code': scale_code,
                        'ticket': ticket,
                        'lines': [],
                    }
                line = {
                    'product': product,
                    'weight': weight,
                    'unit_price': unit_price,
                    'amount': amount,
                    'date_': date_,
                    'time_': time_,
                }
                lines_to_add.append(line)
            new_sale['lines'] = lines_to_add
            new_sales.append(new_sale)
    return new_sales
