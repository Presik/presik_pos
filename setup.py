#!/usr/bin/env python
# This file is part of Presik POS.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from setuptools import setup, find_packages
import os
import glob
import shutil
# from .app import __version__


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


args = {}

data_files = [
    ('app', glob.glob('*.ini')),
    ('app/commons', glob.glob('pos/share/*.css')),
    ('app/share', glob.glob('app/share/*.png')),
    ('app/locale', glob.glob('app/locale/*.qm')),
]

if os.name == 'posix':
    HOME_DIR = os.environ['HOME']
    default_dir = os.path.join(HOME_DIR, '.tryton')

    if not os.path.exists(default_dir):
        os.mkdir(default_dir, '0777')
    default_config = os.path.join(default_dir, 'config_pos.ini')

    if not os.path.exists(default_config):
        shutil.copyfile('config_pos.ini', default_config)
        path_inifile = os.path.join(default_dir, 'config_pos.ini')


setup(
    name='presik_pos',
    # version=__version__,
    description='POS Client for Tryton',
    author='Oscar Alvarez',
    author_email='gerente@presik.com',
    url='www.presik.com',
    download_url="http://www.bitbucket.org",
    packages=[
        'app',
        'app/printing',
        'app/commons',
        'app/css',
        'app/locale',
        'app/tests'
    ],
    data_files=data_files,
    scripts=['pospro'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: Qt',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Natural Language :: English',
        'Natural Language :: Spanish',
        'Programming Language :: Python3.10',
        'Topic :: Office/Business',
    ],
    license='GPL',
    install_requires=[
        'pyusb',
        'pillow',
        'qrcode',
        'paramiko',
        'orjson',
        'escpos',
        'PySide6>=6.4.1',
        'psutil',
        'setproctitle'
    ]
)
